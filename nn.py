import numpy as np

class NeuralNetwork:
    def __init__(self, neurons, activationFunc):
        self.ERROR_NON_SUPPORTED_ACTIVATION_FUNC = "activation function is not supported"
        self.ACTIVATION_STRING = "activation"
        self.nn = []
        
        for idx in range (0, len(neurons)-1):
            self.nn.append( {"inDim": neurons[idx], "outDim": neurons[idx+1], self.ACTIVATION_STRING: activationFunc[idx]} )
    
    def __sigmoid(self, Z):
        return 1/(1+np.exp(-Z))
    
    def __sigmoidDer(self, dA, Z):
        sig = self.__sigmoid(Z)
        return dA*sig*(1-sig)
    
    def __relu(self, Z):
        return np.maximum(0,Z)
    
    def __reluDer(self, dA, Z):
        dZ = np.array(dA, copy=True)
        dZ[Z <= 0] = 0;
        return dZ;
    
    def __getActivationFunc(self, activation):
        if activation is "relu":
            activ = self.__relu
        elif activation is "sigmoid":
            activ = self.__sigmoid
        else:
            raise Exception(self.ERROR_NON_SUPPORTED_ACTIVATION_FUNC)
        
        return activ
    
    def __getActivationDerFunc(self, activation):
        if activation is "relu":
            activDer = self.__reluDer
        elif activation is "sigmoid":
            activDer = self.__sigmoidDer
        else:
            raise Exception(self.ERROR_NON_SUPPORTED_ACTIVATION_FUNC)
        
        return activDer

    def __forwardSingle(self, aPrev, wCurr, bCurr, activation):
        activationFunc = self.__getActivationFunc(activation)
        zCurr = np.dot(wCurr, aPrev) + bCurr
        return activationFunc(zCurr), zCurr
    
    def forward(self, X, params):
        memory = {}
        aCurr = X
        
        for idx, layer in enumerate(self.nn):
            layerIndex = idx + 1
            aPrev = aCurr
            wCurr = params["W" + str(layerIndex)]
            bCurr = params["B" + str(layerIndex)]
            aCurr, zCurr = self.__forwardSingle(aPrev, wCurr, bCurr, layer[self.ACTIVATION_STRING])
            memory["A" + str(idx)] = aPrev
            memory["Z" + str(layerIndex)] = zCurr
           
        return aCurr, memory
    
    def __backwardSingle(self, derACurr, wCurr, zCurr, aPrev, activation):
        activationDerFunc = self.__getActivationDerFunc(activation)
        m = aPrev.shape[1]
        zDerCurr = activationDerFunc(derACurr, zCurr)
        wDerCurr = np.dot(zDerCurr, aPrev.T) / m
        bDerCurr = np.sum(zDerCurr, axis=1, keepdims=True) / m
        aDerCurr = np.dot(wCurr.T, zDerCurr)
    
        return aDerCurr, wDerCurr, bDerCurr
    
    def backward(self, yHat, Y, memory, params):
        gradsVal = {}
        Y = Y.reshape(yHat.shape)
        aDerPrev = - (np.divide(Y, yHat) - np.divide(1-Y, 1-yHat));
        
        for layerPrev, layer in reversed(list(enumerate(self.nn))):
            layerCurr = layerPrev + 1
            activationFunc = layer[self.ACTIVATION_STRING]
            aDerCurr = aDerPrev
            aPrev = memory["A" + str(layerPrev)]
            zCurr = memory["Z" + str(layerCurr)]
            wCurr = params["W" + str(layerCurr)]
            bCurr = params["B" + str(layerCurr)]
            aDerPrev, wDerCurr, bDerCurr = self.__backwardSingle(aDerCurr, wCurr, bCurr, zCurr, aPrev, activationFunc)
            gradsVal["dW" + str(layerCurr)] = wDerCurr
            gradsVal["db" + str(layerCurr)] = bDerCurr
        
        return gradsVal
    
    def cost(self, yHat, Y):
        m = yHat.shape[1]
        cost = -1 / m * (np.dot(Y, np.log(yHat).T) + np.dot(1 - Y, np.log(1 - yHat).T))
        return np.squeeze(cost)
    
    def __convertProbIntoClass(self, probs):
        probs_ = np.copy(probs)
        probs_[probs_ > 0.5] = 1
        probs_[probs_ <= 0.5] = 0
        return probs_
     
    def accuracy(self, yHat, Y):
        yHat_ = self.__convertProbIntoClass(yHat)
        return (yHat_ == Y).all(axis=0).mean()
    
    def update(self, params, grads_values, learning_rate):
        for idx in enumerate(self.nn):
            params["W" + str(idx)] -= learning_rate * grads_values["dW" + str(idx)]        
            params["B" + str(idx)] -= learning_rate * grads_values["db" + str(idx)]
    
        return params;
    
    